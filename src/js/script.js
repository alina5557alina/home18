// Theory in Readme file

// 1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
// Використайте 2 способи для пошуку елементів.
const feature = document.getElementsByClassName('feature');
const featureSelector = document.querySelectorAll('.feature');

console.log(feature);
console.log(featureSelector);

// Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).
featureSelector.forEach(element => {
  element.style.textAlign = 'center';
});

// 2. Змініть текст усіх елементів h2 на "Awesome feature".
const h2 = document.querySelectorAll('h2');
h2.forEach(element => {
  element.innerText = "Awesome feature";
  // console.log(element);
});

// 3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".

const title = document.querySelectorAll('.feature-title');
title.forEach(element => {
  element.innerText += '!';
  // console.log(element);
});

