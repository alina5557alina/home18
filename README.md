# home18

Теоретичні питання
1. Опишіть своїми словами що таке Document Object Model (DOM)
 DOM подібний до карти, яка показує, де щось знаходиться також надає можливість 
 маніпулювати існуючими елементами та створювати нові.

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
Властивість innerHTML дозволяє отримувати HTML всередині елемента у вигляді рядка.
Властивість innerText отримує або встановлює текстовий вміст елемента, враховуючи відображений текст на сторінці.

3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
getElement(s)by або querySelector
getElement(s)by краще використовувати для унікальних елементів(по класу або айді наприклад)ж
querySelector для складних селекторів

4. Яка різниця між nodeList та HTMLCollection?
NodeList може містити будь-які типи вузлів, не лише елементи
HTMLCollection містить лише HTML-елементи
